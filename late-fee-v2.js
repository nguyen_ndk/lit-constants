const CONFIG = [
  // ['purchaseAmountMin', 'purchaseAmountMax', 'flatFeeAmount', 'percentage', 'lateDaysFrequency', 'maxPercentagePurchaseInstallmentFeeAmount'],
  [0, 5000000, 20000, 0.5, 1, 20],
  [5000001, 999999999, 40000, 0.5, 1, 20]
]

const CONFIG_KEY = {}

//Index of CONFIG (Value = Column index of CONFIG array)
CONFIG_KEY['purchaseAmountMin'] = 0
CONFIG_KEY['purchaseAmountMax'] = 1
CONFIG_KEY['flatFeeAmount'] = 2
CONFIG_KEY['percentageFeeAmount'] = 3
CONFIG_KEY['lateDaysFrequency'] = 4
CONFIG_KEY['maxPercentagePurchaseInstallmentFeeAmount'] = 5

const DEADLINE_HOURS = 1 //How many hours after applying late feee


module.exports = {
  CONFIG,
  DEADLINE_HOURS,
  CONFIG_KEY
}