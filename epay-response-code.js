module.exports = {
  SUCCESS: '00_000', //	Successful
  ERROR_FL_900: 'FL_900', //	Connection error.
  ERROR_FL_901: 'FL_901', //	Socket connection error.
  ERROR_FL_902: 'FL_902', //	Some error happened while processing.
  ERROR_FL_903: 'FL_903', //	Socket timeout exception.
  ERROR_OR_101: 'OR_101', //	MerID is invalid or Merchant is not registered. Contact to Customer Service Center for further information.
  ERROR_OR_102: 'OR_102', //	This Payment type doesn’t exist or currently not activated. Contact Customer Service for further information.
  ERROR_OR_103: 'OR_103', //	Currency Code was undefined. Please check your request parameter and make sure [currencyCode] is defined.
  ERROR_OR_104: 'OR_104', //	buyerCity was undefined. Please check your request parameter and make sure [buyerCity] is defined.
  ERROR_OR_105: 'OR_105', //	InvoiceNo was undefined. Please check your request parameter and make sure [invoiceNo] is defined.
  ERROR_OR_106: 'OR_106', //	Goods Name was undefined or wrong format. Please check your request parameter and make sure [goodsNm] is defined.
  ERROR_OR_107: 'OR_107', //	Buyer first name or last name was undefined. Please check your request parameter and make sure [buyerFirstNm] and [buyerLastNm] is defined.
  ERROR_OR_108: 'OR_108', //	Buyer Phone Number was undefined. Please check your request parameter and make sure [buyerPhone] is defined.
  ERROR_OR_109: 'OR_109', //	Buyer Email Address was undefined. Please check your request parameter and make sure [buyerEmail] is defined correctly.
  ERROR_OR_110: 'OR_110', //	Callback URL was undefined. Please check your request parameter and make sure [callbackUrl] is defined.
  ERROR_OR_111: 'OR_111', //	Notification URL was undefined. Please check your request parameter and make sure [notiUrl] is defined.
  ERROR_OR_112: 'OR_112', //	Payment Amount is invalid. Amount should only number and do not includes decimal.
  ERROR_OR_113: 'OR_113', //	Invalid Merchant Token. Contact Customer Service for further information.
  ERROR_OR_114: 'OR_114', //	Payment Amount has to be greater than 0. Please check your request parameter and make sure [amount] is defined.
  ERROR_OR_115: 'OR_115', //	The flag field which was used to identify if a merchant-integration will be checked duplicate order no is null.
  ERROR_OR_116: 'OR_116', //	Invoice no is duplicated
  ERROR_OR_117: 'OR_117', //	Duplicate Merchant transaction ID (merTrxId)
  ERROR_OR_118: 'OR_118', //	Error because of one of the following causes: +) Request domain was undefined +) Total of goods amount (GoodsAmount) and merchant-integration fee (UserFee) is not equal amount of payment (Amount) +) MerchantId sent by merchant-integration is null +) MerchantId is mismatch (Inquiry transaction function)
  ERROR_OR_120: 'OR_120', //	Merchant’s status error (Merchant is not active)
  ERROR_OR_122: 'OR_122', //	Merchant Transaction ID is empty or wrong format
  ERROR_OR_123: 'OR_123', //	Error of merchant-integration is not defined in the system.
  ERROR_OR_124: 'OR_124', //	Merchant’s status error (Merchant is not active)
  ERROR_OR_125: 'OR_125', //	Merchant is not registered this payment method or Cybersource settle time is not defined.
  ERROR_OR_126: 'OR_126', // PG 	Type is not set.
  ERROR_OR_127: 'OR_127', //	Check Merchant’s applied limit error.
  ERROR_OR_128: 'OR_128', //	Over limit amount error.
  ERROR_OR_130: 'OR_130', //	The field used to identify merchant-integration is online or offline is not defined. Please check your request parameter and make sure [merType] is defined
  ERROR_OR_131: 'OR_131', //	This Online Merchant type is currently not activated.
  ERROR_OR_132: 'OR_132', //	This Offline Merchant type is currently not activated.
  ERROR_OR_133: 'OR_133', //	Contract’s information is not defined.
  ERROR_OR_134: 'OR_134', //	Invalid amount
  ERROR_OR_135: 'OR_135', //	Goods Amount is not defined. Please check your request parameter and make sure [goodsAmount] is defined.
  ERROR_OR_136: 'OR_136', //	User Fee is not defined. Please check your request parameter and make sure [userFee] is defined.
  ERROR_OR_140: 'OR_140', //	Transaction does not exist
  ERROR_OR_141: 'OR_141', //	Buyer address is require
  ERROR_OR_142: 'OR_142', //	Buyer state is require when buyer country is 'us' or 'ca'
  ERROR_OR_143: 'OR_143', //	Buyer country is require
  ERROR_OR_147: 'OR_147', //	description is invalid
  ERROR_OR_148: 'OR_148', //	timeStamp is not allowed to be empty or invalid
  ERROR_OR_150: 'OR_150', //	The system doesn’t support tokenization for this payment method
  ERROR_OR_151: 'OR_151', //	userId is not allowed to be empty or is invalid
  ERROR_DC_101: 'DC_101', //	Error ocurred while checking information sending to or receiving from NAPAS
  ERROR_DC_102: 'DC_102', //	Transaction Id is not defined
  ERROR_DC_103: 'DC_103', //	Transaction already exist. Please make new transaction.
  ERROR_DC_104: 'DC_104', //	Invoice no is null. Please make sure that [invoiceNo] was defined already
  ERROR_DC_105: 'DC_105', //	Data is null error.
  ERROR_DC_110: 'DC_110', //	Payment type is undefined. Contact Megapay for further information.
  ERROR_DC_112: 'DC_112', //	Error occurred while inserting or updating data in the tables related to ATM transaction.
  ERROR_DC_113: 'DC_113', //	Error occurred while updating email transaction.
  ERROR_DC_114: 'DC_114', // 	Error when saving data into notify transaction table.
  ERROR_DC_117: 'DC_117', //	Transaction’s information haven’t registered yet. Please check again.
  ERROR_DC_119: 'DC_119', //	Server is busy. Please kindly try again in few minutes.
  ERROR_DC_120: 'DC_120', //	Payment Success but token is not created.
  ERROR_DC_122: 'DC_122', //	Partner transaction Id is null
  ERROR_DC_123: 'DC_123', //	Partner transaction Id is invalid
  ERROR_DC_124: 'DC_124', //	Invalid amount
  ERROR_DC_125: 'DC_125', //	Invalid currency
  ERROR_DC_126: 'DC_126', //	CVV không đúng
  ERROR_IC_101: 'IC_101', //	Transaction failed. Please check card information and try again.
  ERROR_IC_102: 'IC_102', //	Transaction Id is not defined
  ERROR_IC_103: 'IC_103', //	Transaction already exist. Please make new transaction.
  ERROR_IC_104: 'IC_104', //	Invoice no is undefined (null). Please check field named [invoiceNo] again
  ERROR_IC_105: 'IC_105', //	Merchant’s card information is not defined
  ERROR_IC_107: 'IC_107', //	Error occurred while connecting to CyberSource
  ERROR_IC_110: 'IC_110', //	Paytype or merchant-integration id is missing
  ERROR_IC_112: 'IC_112', //	Error occurred while inserting data into tables related to credit card transaction
  ERROR_IC_113: 'IC_113', //	Error occurred while updating email transaction table
  ERROR_IC_115: 'IC_115', //	Invalid MID, Merchant is not registered. Contact Megapay for further information.
  ERROR_IC_117: 'IC_117', //	Transaction’s information is not registered
  ERROR_IC_121: 'IC_121', //	Merchant is inactive
  ERROR_IC_122: 'IC_122', //	payToken is in wrong format or empty
  ERROR_IC_123: 'IC_123', //	Merchant doesn’t support payment with Tokenization with international card
  ERROR_IC_124: 'IC_124', //	Token was not found
  ERROR_IC_125: 'IC_125', //	Token locked
  ERROR_IC_126: 'IC_126', //	Token deleted
  ERROR_IC_127: 'IC_127', //	 Card type (cardTypeValue) is invalid
  ERROR_IC_128: 'IC_128', //	Signature (cardTypeToken) is invalid
  ERROR_IC_129: 'IC_129', //	Card type (cardTypeValue) is not support
  ERROR_VA_101: 'VA_101', //	Connect to VA system is fail or transaction id is not defined
  ERROR_VA_102: 'VA_102', //	VA transaction has already existed
  ERROR_VA_103: 'VA_103', //	Merchant id information is missing
  ERROR_VA_104: 'VA_104', //	Error occurred while inserting data into VA trans table
  ERROR_VA_105: 'VA_105', //	Error occurred while inserting data into trans result table.
  ERROR_VA_106: 'VA_106', //	Error occurred while searching transaction or transaction doesn’t exist
  ERROR_VA_107: 'VA_107', //	Error occurred while inserting data into merchant-integration notification table
  ERROR_VA_109: 'VA_109', //	Condition of receiving money is wrong (should equal to 03)
  ERROR_VA_110: 'VA_110', //	Effective date is wrong
  ERROR_VA_111: 'VA_111', //	Expire date is wrong
  ERROR_VA_112: 'VA_112', //	Transaction is invalid
  ERROR_VA_113: 'VA_113', //	Can not find Bank issuing Virtual account
  ERROR_CC_101: 'CC_101', //	Transaction id is not generated.
  ERROR_CC_102: 'CC_102', //	Invalid MID or Merchant is not registered. Contact Customer Service for further information.
  ERROR_CC_109: 'CC_109', //	Merchant in the not active status
  ERROR_CC_110: 'CC_110', //	The transaction has not been registered
  ERROR_CC_111: 'CC_111', //	Error occurred when canceling amount less than or equal 0 or canceling amount not equal transaction amount (In case of comprehensive cancelation)
  ERROR_CC_112: 'CC_112', //	The transaction need to be canceled is not found.
  ERROR_CC_113: 'CC_113', //	The transaction has been canceled fully
  ERROR_CC_114: 'CC_114', //	This payment method is currently not activated with merchant-integration or insert notification data has been failed
  ERROR_CC_115: 'CC_115', //	Merchant’s token is invalid
  ERROR_CC_116: 'CC_116', //	Canceling amount must equal payment amount.
  ERROR_CC_117: 'CC_117', //	Amount of cancelation/refunding is invalid (Format number exception)
  ERROR_CC_118: 'CC_118', // 	It has been canceled.
  ERROR_CC_119: 'CC_119', //	The amount of money you entered is larger than the payment amount or the remaining payment amount is less than 0.
  ERROR_CC_121: 'CC_121', //	Error occurred when updating transaction’s information
  ERROR_CC_122: 'CC_122', //	Error occurred when inserting partial cancelation information
  ERROR_CC_124: 'CC_124', //	Error occurred when inserting data after canceling
  ERROR_CC_125: 'CC_125', //	Transaction result registration error.
  ERROR_CC_126: 'CC_126', //	Error occurred when quering data
  ERROR_CC_127: 'CC_127', //	Partial cancelation flag or status is invalid.
  ERROR_CC_128: 'CC_128', //	Cancel message is not defined
  ERROR_CC_130: 'CC_130', //	The amount of money you want to cancel is smaller than the transaction’s payment amount.
  ERROR_CC_131: 'CC_131', //	Error occurred when inserting data into cancelation transaction table
  ERROR_CC_132: 'CC_132', //	Update transaction history error.
  ERROR_CC_133: 'CC_133', //	Bank connection error.
  ERROR_CC_135: 'CC_135', //	Cancel password is mismatched.
  ERROR_CC_136: 'CC_136', //	Canceling function is unavailable with this merchant-integration. Please Contact to Meagepay.
  ERROR_CC_141: 'CC_141', //	Currently can not partial refund for this transaction, please wait until tomorrow
  ERROR_CC_143: 'CC_143', //	This transaction must refund all
  ERROR_PG_ER1: 'PG_ER1', //	Transaction Failed.
  ERROR_PG_ER2: 'PG_ER2', //	Card’s information is wrong
  ERROR_PG_ER3: 'PG_ER3', //	Transaction is failed - Timeout
  ERROR_PG_ER4: 'PG_ER4', //	Transaction is failed
  ERROR_PG_ER5: 'PG_ER5', //	Customer canceled transaction
  ERROR_PG_ER6: 'PG_ER6', //	System error, please contact to Megapay’s Admin for supporting
  ERROR_PG_ER7: 'PG_ER7', //	Card number is invalid
  ERROR_PG_ER8: 'PG_ER8', //	Publish/Expire date is invalid
  ERROR_PG_ER10: 'PG_ER10', //	Buyer address is wrong
  ERROR_PG_ER11: 'PG_ER11', //	 Card has not been configured Payer Authentication yet
  ERROR_PG_ER12: 'PG_ER12', //	Buyer last name or first name is wrong
  ERROR_PG_ER13: 'PG_ER13', //	Buyer city/state is wrong
  ERROR_PG_ER16: 'PG_ER16', //	OTP is wrong
  ERROR_PG_ER17: 'PG_ER17', //	Card information has not been approved yet, please contact to issuing bank to be supported
  ERROR_PG_ER18: 'PG_ER18', //	Card expired or locked
  ERROR_PG_ER19: 'PG_ER19', //	The amount of money is not enough to make a payment
  ERROR_PG_ER20: 'PG_ER20', //	The amount of money of transaction is not within the allowed limit
  ERROR_PG_ER21: 'PG_ER21', //	Card has not been activated or signed up for online payment yet
  ERROR_PG_ER22: 'PG_ER22', //	Card holder name is wrong
  ERROR_PG_ER23: 'PG_ER23', //	Issuing bank denied the transaction
  ERROR_PG_ER25: 'PG_ER25', //	The transaction was denied by fraud management system
  ERROR_PG_ER26: 'PG_ER26', //	Data is invalid or empty
  ERROR_PG_ER28: 'PG_ER28', //	Issuing bank is postponing this transaction. Please try again later
  ERROR_PG_ER29: 'PG_ER29', //	Transaction failed because customer is in black list
  ERROR_PG_ER30: 'PG_ER30', //	Transaction failed – Cannot authenticate the customer
  ERROR_PG_ER42: 'PG_ER42', //	OTP time out (if you are charged, it will be refunded)
  ERROR_PG_ER43: 'PG_ER43', //	Issuing bank is busy. Please try again
}