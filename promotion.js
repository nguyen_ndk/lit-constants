const TYPE = {
  PERCENTAGE: 1,
  AMOUNT: 2,
}

const STATUS = {
  SUCCESS: 'SUCCESSFULLY'
}

module.exports = {
  TYPE,
  STATUS
}